CCtzn.calc = (function() {

  // Class Plan
  function Plan() {
    this.flags = {
      complete: true,
      tight: false
    };
    this.gaps = 0;
    this.items = [];
  }

  function _timestampToTimeString(timestamp) {
    var	date = new Date(timestamp * 1000),
      hours = ('0' + date.getHours()).slice(-2),
      minutes = ('0' + date.getMinutes()).slice(-2);

    return hours + ':' + minutes;
  }

  // Filter function
  function _laterThan(timestamp) {
    return (timestamp >= this);
  }

  function _getSinglePlan(data, favourite, mode) {
    var	data = data.slice(0),
      dataLength = data.length,
      dataHours = [],
      firstLoop = true,
      endTimestamp = 0,
      relativeEndTimestamp = 0,
      plan = new Plan();

    while (data.length) {
      var earliests = [];
      for (var j = 0; j < data.length; j++) {
        if (firstLoop) {
          dataHours[j] = data[j]['hours'].slice(0);
        }
        earliests.push(dataHours[j][0] || Infinity);
      }

      var earliest = (firstLoop) ? favourite : earliests.indexOf(Math.min.apply(null, earliests));
      if (isFinite(earliests[earliest])) {
        if (!firstLoop) {
          plan['gaps'] += Math.max((earliests[earliest] - endTimestamp), 0);
        }
        endTimestamp = earliests[earliest] + data[earliest]['duration'] + CCtzn.config.commercialsLength;
        relativeEndTimestamp = endTimestamp - CCtzn.config.commercialsLength + CCtzn.config.transferLength[mode];
        var item = {
          title: data[earliest]['title'],
          start: _timestampToTimeString(earliests[earliest]),
          end: _timestampToTimeString(endTimestamp)
        };
        plan['items'].push(item);
      }

      data.splice(earliest, 1);
      dataHours.splice(earliest, 1);
      firstLoop = false;

      for (var j = 0; j < dataHours.length; j++) {
        dataHours[j] = dataHours[j].filter(_laterThan, relativeEndTimestamp);
      }
    }

    if (plan.items.length < dataLength) {
      plan.flags.complete = false;
    }
    if (plan.gaps < (plan.items.length - 1) * CCtzn.config.transferLength) {
      plan.flags.tight = true;
    }

    return plan;
  }

  function _getAllPlans(data, expertMode) {
    var	allPlans = [],
      dataLength = data.length,
      mode = (expertMode) ? 'expert' : 'comfortable';

    for (var i = 0; i < dataLength; i++) {
      allPlans.push(_getSinglePlan(data, i, mode));
    }

    return allPlans;
  }

  // Sort function
  // Plans containing most movies and least gaps go first.
  function _sortPlans(planA, planB) {
    if (planA.flags.complete === planB.flags.complete) {
      if (planA.flags.complete || planA.items.length === planB.items.length) {
        return planA.gaps - planB.gaps;
      }
      else {
        return planA.items.length - planB.items.length;
      }
    }
    else if (!planA.flags.complete) {
      return 1;
    }
    else {
      return -1;
    }
  }

  function _getCalculatedPlans(data, expertMode) {
    var	bestPlans = [],
      allPlans = _getAllPlans(data, expertMode);
    allPlans.sort(_sortPlans);

    for (var i = 0; i < CCtzn.config.bestPlansAmount; i++) {
      allPlans[i] && bestPlans.push(allPlans[i]);
    }

    return bestPlans;
  }

  function _getSingleMoviePlan(data) {
    var	_data = data[0],
      plan = new Plan(),
      i = 0;

    for (; i < _data.hours.length; i++) {
      var	endTimestamp = _data.hours[i] + _data.duration + CCtzn.config.commercialsLength,
        item = {
          title: _data.title,
          start: _timestampToTimeString(_data.hours[i]),
          end: _timestampToTimeString(endTimestamp)
        };
      plan['items'].push(item);
    }

    return plan;
  }

  return {
    getCalculatedPlans: _getCalculatedPlans,
    getSingleMoviePlan: _getSingleMoviePlan
  };

}());
