CCtzn.config = {

  message: {
    warning: {
      incomplete: 'Nie zmieściły się wszystkie filmy.',
      tight: 'Bardzo krótkie odstępy pomiędzy seansami.'
    },
    error: {
      dataNotFound: 'Nie mamy repertuaru na ten dzień :( Spróbuj ponownie później lub podaj inną datę.',
      noMovies: 'Brak filmów w wybranym dniu. Podaj inną datę.',
      timeTooLate: 'Brak filmów o wybranej porze. Podaj wcześniejszy czas.',
      moviesTooFew: 'Zaznacz przynajmniej jeden film.'
    }
  },

  commercialsLength: 25 * 60,
  transferLength: {
    comfortable: 10 * 60,
    expert: -5 * 60
  },
  bestPlansAmount: 2,

  achievementsTotal: 35,
  achievementsCinemasAll: {
    cc: ['cc1088', 'cc1086', 'cc1092', 'cc1098', 'cc1089', 'cc1075', 'cc1085', 'cc1065', 'cc1079', 'cc1090', 'cc1076', 'cc1063', 'cc1064', 'cc1094', 'cc1084', 'cc1080', 'cc1081', 'cc1078', 'cc1062', 'cc1082', 'cc1083', 'cc1095', 'cc1077', 'cc1093', 'cc1091', 'cc1074', 'cc1061', 'cc1096', 'cc1070', 'cc1069', 'cc1068', 'cc1060', 'cc1067', 'cc1097', 'cc1087'],
    mk: ['mk3', 'mk37', 'mk2', 'mk4', 'mk5', 'mk39', 'mk33', 'mk6', 'mk7', 'mk38', 'mk21', 'mk40', 'mk8', 'mk9', 'mk10', 'mk26', 'mk25', 'mk11', 'mk30', 'mk32', 'mk12', 'mk13', 'mk35', 'mk43', 'mk14', 'mk19', 'mk15', 'mk1', 'mk16', 'mk17', 'mk18', 'mk20', 'mk34'],
    h: ['h37', 'h26', 'h6', 'h46', 'h7', 'h38', 'h27', 'h2', 'h58', 'h49', 'h42', 'h9', 'h10', 'h32', 'h45', 'h40', 'h51', 'h34', 'h13', 'h14', 'h55', 'h61', 'h30', 'h31', 'h47', 'h39', 'h16', 'h17', 'h60', 'h28', 'h19', 'h29', 'h50', 'h52', 'h20', 'h36', 'h21', 'h43', 'h22', 'h56', 'h44', 'h23', 'h35', 'h33', 'h53', 'h57', 'h54', 'h48', 'h3', 'h64'],
    c3d: ['c3dbiala-podlaska', 'c3dgdansk', 'c3dglogow', 'c3dgorzow-wlkp', 'c3dkalisz', 'c3dklodzko', 'c3dleszno', 'c3dmielec', 'c3dswidnica', 'c3dswinoujscie', 'c3dtarnow', 'c3dwarszawa-atrium-reduta'],
    warszawa: ['cc1074', 'cc1061', 'cc1096', 'cc1070', 'cc1069', 'cc1068', 'cc1060', 'mk43', 'mk14', 'mk19', 'mk15', 'mk1', 'h57', 'c3dwarszawa-atrium-reduta'],
    poznan: ['cc1081', 'cc1078', 'mk8', 'mk9', 'mk10', 'h50'],
    wroclaw: ['cc1067', 'cc1097', 'mk17', 'mk18', 'h48', 'h3'],
    krakow: ['cc1090', 'cc1076', 'cc1063', 'cc1064', 'mk7'],
    gdansk: ['mk4', 'h2', 'h58', 'h49', 'c3dgdansk'],
    katowice: ['cc1065', 'cc1079', 'mk39', 'h51'],
    bialystok: ['h26', 'h6', 'h46'],
    bydgoszcz: ['cc1086', 'mk3', 'h38'],
    lublin: ['cc1094', 'cc1084', 'mk38'],
    lodz: ['cc1080', 'mk21', 'h47'],
    rzeszow: ['mk30', 'h36', 'h21'],
    szczecin: ['mk13', 'h23', 'h35'],
  },
  achievementsCinemasAllId: {
    all: 11,
    cc: 8,
    mk: 9,
    h: 10,
    c3d: 37,
    warszawa: 2,
    krakow: 3,
    poznan: 4,
    katowice: 5,
    lublin: 6,
    wroclaw: 7,
    bydgoszcz: 31,
    gdansk: 32,
    lodz: 33,
    bialystok: 34,
    szczecin: 35,
    rzeszow: 36,
  }

};

CCtzn.config.achievementsCinemasAll.all = [].concat(
  CCtzn.config.achievementsCinemasAll.cc,
  CCtzn.config.achievementsCinemasAll.mk,
  CCtzn.config.achievementsCinemasAll.h,
  CCtzn.config.achievementsCinemasAll.c3d
);
