CCtzn.achievements = (function() {

  var _achievements,
      _achievementsStats = {},
      $achievementsCount = $('#achievements-count'),
      $achievementsItems = $('[data-achievement-id]');

  function _updateUI(isInit, addedAchievementId) {
    $achievementsCount.text(_achievements.length + '/' + CCtzn.config.achievementsTotal);
    if (isInit) {
      _achievements.forEach(function(achievementId) {
        $achievementsItems.filter('[data-achievement-id="' + achievementId + '"]').addClass('unlocked');
      });
    }
    else {
      $achievementsItems.filter('[data-achievement-id="' + addedAchievementId + '"]').addClass('just unlocked');
    }
  }

  function _addAchievement(achievementId) {
    if (_achievements.indexOf(achievementId) === -1) {
      _achievements.push(achievementId);
      localStorage.setItem('achievements_state', JSON.stringify(_achievements));
      _updateUI(false, achievementId);
    }
  }

  function _trigger(eventType, eventData) {
    switch (eventType) {
      case 'pageEntered':
        _addAchievement(1);
        break;
      case 'cinemaPicked':
        var	cinemaCode = eventData.cinemaChain + eventData.cinemaId,
          cinemasAllKeys = Object.keys(CCtzn.config.achievementsCinemasAll),
          i = 0;
        for (; i < cinemasAllKeys.length; i++) {
          if (
            CCtzn.config.achievementsCinemasAll[cinemasAllKeys[i]].indexOf(cinemaCode) !== -1
            && _achievementsStats.cinemasAll[cinemasAllKeys[i]].indexOf(cinemaCode) === -1
          ) {
            var storageKey = 'achievements_state_cinemas_all_' + cinemasAllKeys[i];
            _achievementsStats.cinemasAll[cinemasAllKeys[i]].push(cinemaCode);
            localStorage.setItem(storageKey, JSON.stringify(_achievementsStats.cinemasAll[cinemasAllKeys[i]]));
            if (_achievementsStats.cinemasAll[cinemasAllKeys[i]].length === CCtzn.config.achievementsCinemasAll[cinemasAllKeys[i]].length) {
              _addAchievement(CCtzn.config.achievementsCinemasAllId[cinemasAllKeys[i]]);
            }
          }
        }
        break;
      case 'timetableGenerated':
        if (
          (eventData.expertMode || eventData.tight)
          && eventData.moviesAmount > 2
          && eventData.moviesAmount <= 6
        ) {
          _addAchievement(14 + eventData.moviesAmount);
        }
        else if (eventData.moviesAmount !== 2 && eventData.moviesAmount <= 6) {
          if (eventData.moviesAmount === 1) {
            _addAchievement(12);
          }
          else {
            _addAchievement(10 + eventData.moviesAmount);
          }
        }
        if (eventData.start.dateTime.getHours() < 12) {
          _addAchievement(21);
        }
        if (eventData.start.dateTime.getDay() === 5 && eventData.start.dateTime.getDate() === 13) {
          _addAchievement(22);
        }
        if (
          eventData.start.timestamp - eventData.now.timestamp <= 1800
          && eventData.start.timestamp - eventData.now.timestamp >= 0
        ) {
          _addAchievement(26);
        }
        else if (eventData.start.timestamp - eventData.now.timestamp >= 432000) {
          _addAchievement(27);
        }
        if (_achievementsStats.timetablesAll.days.indexOf(eventData.start.dateTime.getDay()) === -1) {
          _achievementsStats.timetablesAll.days.push(eventData.start.dateTime.getDay());
          localStorage.setItem('achievements_state_timetables_all_days', JSON.stringify(_achievementsStats.timetablesAll.days));
          if (_achievementsStats.timetablesAll.days.length === 7) {
            _addAchievement(24);
          }
        }
        if (_achievementsStats.timetablesAll.months.indexOf(eventData.start.dateTime.getMonth()) === -1) {
          _achievementsStats.timetablesAll.months.push(eventData.start.dateTime.getMonth());
          localStorage.setItem('achievements_state_timetables_all_months', JSON.stringify(_achievementsStats.timetablesAll.months));
          if (_achievementsStats.timetablesAll.months.length === 12) {
            _addAchievement(25);
          }
        }
        break;
      case 'alternativeTimetableGenerated':
        _achievementsStats.alternativeTimetablesCount++;
        if (_achievementsStats.alternativeTimetablesCount >= 100) {
          _addAchievement(30);
        }
        else {
          if (_achievementsStats.alternativeTimetablesCount >= 20) {
            _addAchievement(29);
          }
          else if (_achievementsStats.alternativeTimetablesCount >= 5) {
            _addAchievement(28);
          }
          localStorage.setItem('achievements_state_alt_timetables_count', _achievementsStats.alternativeTimetablesCount);
        }
        break;
    }
  }

  function _init() {
    _achievements = JSON.parse(localStorage.getItem('achievements_state'));
    if (!_achievements) {
      _achievements = [];
      localStorage.setItem('achievements_state', JSON.stringify(_achievements));
    }
    _achievementsStats = {
      alternativeTimetablesCount: parseInt(localStorage.getItem('achievements_state_alt_timetables_count')) || 0,
      cinemasAll: {
        all: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_all')) || [],
        cc: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_cc')) || [],
        mk: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_mk')) || [],
        h: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_h')) || [],
        c3d: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_c3d')) || [],
        warszawa: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_warszawa')) || [],
        poznan: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_poznan')) || [],
        wroclaw: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_wroclaw')) || [],
        krakow: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_krakow')) || [],
        gdansk: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_gdansk')) || [],
        bialystok: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_bialystok')) || [],
        bydgoszcz: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_bydgoszcz')) || [],
        katowice: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_katowice')) || [],
        lublin: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_lublin')) || [],
        lodz: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_lodz')) || [],
        rzeszow: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_rzeszow')) || [],
        szczecin: JSON.parse(localStorage.getItem('achievements_state_cinemas_all_szczecin')) || [],
      },
      timetablesAll: {
        days: JSON.parse(localStorage.getItem('achievements_state_timetables_all_days')) || [],
        months: JSON.parse(localStorage.getItem('achievements_state_timetables_all_months')) || []
      },
    };
    _achievements.length && _updateUI(true);
  }

  return {
    init: _init,
    trigger: _trigger
  };

}());

CCtzn.achievements.init();
