CCtzn.ui = (function() {

  // Messages handler

  function _setMessage(level, codes) {
    var messageMarkup = '';

    for (var i = 0; i < codes.length; i++) {
      messageMarkup +=	'<p>' +
                  CCtzn.config.message[level][codes[i]] +
                '</p>';
    }

    $('#' + level).find('p').remove();
    $('#' + level).append(messageMarkup).popup('open');
  }

  // Event handlers

  function _pageContainerBeforeChangeHandler(e, d) {
    if (typeof d.toPage === 'string' && d.toPage.indexOf('#time') !== -1) {
      var showExpertMode = !CCtzn.options.getValue('expertmode_alwaysenabled');
      $('#expert-wrapper').toggle(showExpertMode);
      $('#alternative').data({
        set: 1,
        used: false
      });
    }
    else if (typeof d.toPage === 'string' && d.toPage.indexOf('#movies') !== -1) {
      var	canSkip = CCtzn.recent.canSkipCinemaPicker(),
        favourite = (canSkip) ? (' w ' + CCtzn.recent.getCinemaData()['cinemaName']) : '',
        $moviesNavbar = $('#movies-navbar');

      $('#favourite-page').text(favourite);
      if (!canSkip) {
        $moviesNavbar.find('.change').remove();
      }
      else if (!$moviesNavbar.find('.change').length) {
        $moviesNavbar.find('ul').prepend('<li class="change"><a href="#cinema" class="ui-btn cctzn-btn">Zmień kino</a></li>');
      }
      if ($moviesNavbar.data('mobileNavbar')) {
        $moviesNavbar.find('ul').removeClass('ui-grid-a ui-grid-solo');
        $moviesNavbar.find('li').removeClass('ui-block-a ui-block-b');
        $moviesNavbar.navbar('destroy').navbar();
      }
    }
    else if (typeof d.toPage === 'string' && d.toPage.indexOf('#options') !== -1) {
      $('#options').find('[data-option]').filter('[type=checkbox]').each(function() {
        var	$this = $(this),
          checked = CCtzn.options.getValue($this.data('option'));

        $this.prop('checked', checked)
        if ($this.data('checkboxradio')) {
          $this.checkboxradio('refresh');
        }
      });
    }
    else if (typeof d.toPage === 'string' && d.toPage.indexOf('#achievements') !== -1) {
      CCtzn.achievements.trigger('pageEntered');
    }
  }

  function _pageContainerChangeHandler() {
    var currentTitle = $('.ui-page-active').jqmData('title');
    $('[data-role=header] h2').text(currentTitle);

    if (currentTitle === 'Kino' && CCtzn.recent.getState() === 'selected_twice') {
      $('#favourite-popup').text(CCtzn.recent.getCinemaData()['cinemaName']);
      $('#confirmation').popup('open');
    }
  }

  var _start = {};

  function _setStart() {
    _start.date = $('input[name=start-date]:checked').val();
    _start.time = $('#start-time').val();
    _start.dateTime = new Date(_start.date.substr(4, 2) + '/' + _start.date.substr(6) + '/' + _start.date.substr(0, 4) + ' ' + _start.time);
    _start.timestamp = Math.ceil(_start.dateTime.getTime() / 1000);

    return _start;
  }

  function _forwardButtonHandler() {
    if (CCtzn.recent.canSkipCinemaPicker()) {
      var cinemaData = CCtzn.recent.getCinemaData();

      $.mobile.loading('show');
      CCtzn.cinemaPicked(cinemaData, _setStart());
    }
    else {
      $('body').pagecontainer('change', '#cinema');
    }
  }

  function _cinemaPickerHandler() {
    var $this = $(this),
        cinemaData = $this.closest('li').data();

    $.mobile.loading('show');
    !CCtzn.options.getValue('recentcinema_disabled') && CCtzn.recent.cinemaPicked(cinemaData);
    CCtzn.achievements.trigger('cinemaPicked', cinemaData);
    CCtzn.cinemaPicked(cinemaData, _setStart());
  }

  function _confirmButtonHandler() {
    var cinemaData = CCtzn.recent.getCinemaData();

    $.mobile.loading('show');
    CCtzn.recent.cinemaConfirmed();
    CCtzn.cinemaPicked(cinemaData, _setStart());
  }

  var _expertMode = false;

  function _generateButtonHandler() {
    _expertMode = $('#expert').prop('checked') || CCtzn.options.getValue('expertmode_alwaysenabled');
    var selectedMovies = {
      ids: [],
      names: []
    };

    $.mobile.loading('show');

    $('#movie-picker').find('input:checked').each(function() {
      selectedMovies['ids'].push(parseInt($(this).val()));
      selectedMovies['names'].push($(this).siblings('label').text());
    });

    if (!selectedMovies['ids'].length) {
      $.mobile.loading('hide');
      _setMessage('error', ['moviesTooFew']);
    }
    else {
      CCtzn.generateTimetable(selectedMovies, _expertMode);
    }
  }

  function _alternativeButtonHandler() {
    var $this = $(this),
        btnData = $this.data(),
        nextSet = (btnData.set + 1) % CCtzn.config.bestPlansAmount;

    CCtzn.generateAlternativeTimetable(btnData.set);
    btnData.used || CCtzn.achievements.trigger('alternativeTimetableGenerated');
    $this.data({
      set: nextSet,
      used: true
    });
  }

  function _cinemaPickedErrorHandler(codes) {
    $('body').pagecontainer('change', '#time');
    $.mobile.loading('hide');
    _setMessage('error', codes);
  }

  // Rendering functions

  function _setTimePicker() {
    var	timeBorders = CCtzn.getTimeBorders();

    if (timeBorders.min.getHours() < 8 || timeBorders.min.getHours() >= 20) {
      $('#start-time').val('10:00');
    }
    else {
      $('#start-time').val(timeBorders.min.getHours() + 2 + ':00');
    }
  }

  function _renderDatePicker() {
    var	timeBorders = CCtzn.getTimeBorders(),
      pickerDate = new Date(timeBorders.min.getTime() - ((timeBorders.min.getDay() || 7) - 1) * 86400000),
      pickerMarkup = '';

    for (var i = 0; i < 14; i++) {
      var	dateString = ('0' + pickerDate.getDate()).slice(-2),
        monthString = ('0' + (pickerDate.getMonth() + 1)).slice(-2),
        dateValue = pickerDate.getFullYear() + monthString + dateString,
        isDisabled = (
          (pickerDate < timeBorders.min && pickerDate.getDate() !== timeBorders.min.getDate())
          || (pickerDate > timeBorders.max && pickerDate.getDate() !== timeBorders.max.getDate())
        ) ? ' disabled ' : '',
        isChecked = (
          (pickerDate.getDate() === timeBorders.min.getDate() && timeBorders.min.getHours() < 20)
          || (new Date(pickerDate.getTime() - 86400000).getDate() === timeBorders.min.getDate() && timeBorders.min.getHours() >= 20)
        ) ? ' checked="checked" ' : '';

      pickerMarkup +=	'<input name="start-date" id="start-date-' + dateValue + '" value="' + dateValue + '" type="radio"' + isDisabled + isChecked + '>' +
              '<label for="start-date-' + dateValue + '">' + dateString + '<small>' + monthString + '</small></label>';

      pickerDate = new Date(pickerDate.getTime() + 86400000);

      if (pickerDate.getDay() === 1 && pickerDate > timeBorders.max) {
        break;
      }
    }

    $('#start-date').append(pickerMarkup);

    _setTimePicker();
  }

  function _renderMoviePicker(movieData) {
    var	$moviePicker = $('#movie-picker'),
      pickerMarkup = '';

    for (var i = 0; i < movieData.length; i++) {
      var labelsMarkup = '',
        movieLabels = movieData[i]['labels'];

      if (movieLabels.length) {
        labelsMarkup += '<br>';
        for (var j = 0; j < movieLabels.length; j++) {
          labelsMarkup += '<span class="cctzn-btn cctzn-label">' + movieLabels[j] + '</span>';
        }
      }

      pickerMarkup +=	'<label>' +
                '<input type="checkbox" value="' + i + '">' +
                '<span>' +
                  movieData[i]['title'] +
                '</span>' +
                labelsMarkup +
              '</label>';
    }

    if ($moviePicker.data('mobileControlgroup')) {
      $moviePicker.controlgroup('container').html(pickerMarkup);
      $moviePicker.enhanceWithin().controlgroup('refresh');
    }
    else {
      $moviePicker.html(pickerMarkup);
    }
    $('body').pagecontainer('change', '#movies');
    $.mobile.loading('hide');
  }

  function _getPlanMarkup(planData, isSingle) {
    var planMarkup = '';

    for (var i = 0; i < planData['items'].length; i++) {
      var itemTitle = (isSingle) ? '' : '<br>' + planData['items'][i]['title'];
      planMarkup +=	'<li>' +
                '<span>' +
                  planData['items'][i]['start'] +
                  ' ~ ' +
                  planData['items'][i]['end'] +
                '</span>' +
                itemTitle +
              '</li>';
    }

    return planMarkup;
  }

  // Class AchievementTimetableGeneratedEventData
  function AchievementTimetableGeneratedEventData(planData, isSingle) {
    this.moviesAmount = (isSingle) ? 1 : planData.items.length;
    this.expertMode = _expertMode;
    this.tight = planData.flags.tight;
    this.start = _start;
    this.now = {
      dateTime: CCtzn.getTimeBorders().min,
      timestamp: Math.ceil(CCtzn.getTimeBorders().min.getTime() / 1000)
    };
  }

  function _renderSingleMoviePlan(planData) {
    var	planMarkup = _getPlanMarkup(planData, true),
      achievementEventData = new AchievementTimetableGeneratedEventData(planData, true);

    $('#selected').text(planData['items'][0]['title']);
    $('#screenings').html(planMarkup);
    $('body').pagecontainer('change', '#single');
    $.mobile.loading('hide');

    CCtzn.achievements.trigger('timetableGenerated', achievementEventData);
  }

  function _renderCalculatedPlan(planData, set, noWarning) {
    var	set = set || 0,
      noWarning = noWarning || false,
      warnings = [],
      planMarkup = '',
      achievementEventData = (noWarning) ? {} : new AchievementTimetableGeneratedEventData(planData[set]);

    if (!planData[set]) {
      set = planData.length - 1;
    }
    if (!planData[set]['flags']['complete']) {
      warnings.push('incomplete');
    }
    if (planData[set]['flags']['tight']) {
      warnings.push('tight');
    }
    planMarkup = _getPlanMarkup(planData[set]);

    $('#timetable').html(planMarkup);
    $('body').pagecontainer('change', '#plan');
    $.mobile.loading('hide');
    if (warnings.length && !noWarning) {
      CCtzn.ui.setMessage('warning', warnings);
    }

    noWarning || CCtzn.achievements.trigger('timetableGenerated', achievementEventData);
  }

  // Initialization

  function _setUI() {
    $.mobile.defaultPageTransition = 'slide';
    $('#menu').children('ul').listview().end().panel();
    $('[data-role=header]').toolbar();
    $('#warning, #error, #confirmation').popup({ overlayTheme: 'a' });
  }

  function _observeUI() {
    $(document).on('pagecontainerbeforechange', _pageContainerBeforeChangeHandler);
    $(document).on('pagecontainerchange', _pageContainerChangeHandler);
    $('#forward').on('click', _forwardButtonHandler);
    $('#cinema-picker').on('click', 'a', _cinemaPickerHandler);
    $('#confirm').on('click', _confirmButtonHandler);
    $('#generate').on('click', _generateButtonHandler);
    $('#alternative').on('click', _alternativeButtonHandler);
  }

  function _init() {
    $(function() {
      _setUI();
      _observeUI();
    });
  }

  return {
    setMessage: _setMessage,
    cinemaPickedErrorHandler: _cinemaPickedErrorHandler,
    renderDatePicker: _renderDatePicker,
    renderMoviePicker: _renderMoviePicker,
    renderSingleMoviePlan: _renderSingleMoviePlan,
    renderCalculatedPlan: _renderCalculatedPlan,
    init: _init
  };

}());

CCtzn.ui.init();
