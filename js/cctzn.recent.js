CCtzn.recent = (function() {

  /*
   *	Recent cinema memory "states":
   *	Value				      Description					      UI Effect
   *
   *	'never_selected'	Never selected a cinema		None
   *	'selected_once'		Selected a cinema			    None
   *	'selected_twice'	Selected the same cinema	Display confirmation dialog, a change reverts to 'selected_once'
   *	'confirmed'			  Confirmed "fav" cinema		Skip cinema selection and show "change" button, change reverts to 'selected_once'
   */

  function _getState() {
    return localStorage.getItem('recent_cinema_state') || 'never_selected';
  }

  if (_getState() === 'never_selected') {
    localStorage.setItem('recent_cinema_state', 'never_selected');
  }

  function _getCinemaData() {
    var cinemaData = {
      cinemaChain: localStorage.getItem('recent_cinema_chain'),
      cinemaId: parseInt(localStorage.getItem('recent_cinema_id')),
      cinemaName: localStorage.getItem('recent_cinema_name')
    };

    return cinemaData;
  }

  function _cinemaPicked(cinemaData) {
    var current = {
        state: _getState(),
        chain: _getCinemaData()['cinemaChain'],
        id: _getCinemaData()['cinemaId'],
        name: _getCinemaData()['cinemaName']
    };

    localStorage.setItem('recent_cinema_chain', cinemaData.cinemaChain);
    localStorage.setItem('recent_cinema_id', cinemaData.cinemaId);
    localStorage.setItem('recent_cinema_name', cinemaData.cinemaName);

    switch (current.state) {
      case 'never_selected':
      case 'selected_twice':
        localStorage.setItem('recent_cinema_state', 'selected_once');
        break;
      case 'selected_once':
        if (current.chain === cinemaData.cinemaChain && current.id === cinemaData.cinemaId) {
          localStorage.setItem('recent_cinema_state', 'selected_twice');
        }
        break;
      case 'confirmed':
        if (current.chain !== cinemaData.cinemaChain || current.id !== cinemaData.cinemaId) {
          localStorage.setItem('recent_cinema_state', 'selected_once');
        }
        break;
    }
  }

  function _cinemaConfirmed() {
    localStorage.setItem('recent_cinema_state', 'confirmed');
  }

  function _clearState() {
    localStorage.setItem('recent_cinema_state', 'never_selected');
    localStorage.removeItem('recent_cinema_chain');
    localStorage.removeItem('recent_cinema_id');
    localStorage.removeItem('recent_cinema_name');
  }

  function _canSkipCinemaPicker() {
    return (_getState() === 'confirmed');
  }

  return {
    getState: _getState,
    getCinemaData: _getCinemaData,
    cinemaPicked: _cinemaPicked,
    cinemaConfirmed: _cinemaConfirmed,
    clearState: _clearState,
    canSkipCinemaPicker: _canSkipCinemaPicker
  };

}());
