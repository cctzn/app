window.CCtzn = (function() {

  // Movie data

  var _movieData = [],
    _cachedMovieData = '',
    _cachedFileName = '';

  function _getFilename(cinemaData, startDate) {
    var fileName = 'data/' + cinemaData.cinemaChain + '_' + cinemaData.cinemaId + '_' + startDate + '.json';

    return fileName;
  }

  function _laterThan(timestamp) {
    return (timestamp >= this);
  }

  function _isEmpty(item) {
    return (item['hours'].length);
  }

  function _filterMovieDataByStartDateTime(movieData, startTimestamp) {
    for (var i = 0; i < movieData.length; i++) {
      movieData[i]['hours'] = movieData[i]['hours'].filter(_laterThan, startTimestamp);
    }
    movieData = movieData.filter(_isEmpty);

    return movieData;
  }

  function _cinemaPickedSuccessHandler(movieData, startData) {
    _movieData = movieData;

    if (!_movieData.length) {
      CCtzn.ui.cinemaPickedErrorHandler(['noMovies']);
    }
    else {
      _movieData = _filterMovieDataByStartDateTime(_movieData, startData.timestamp);
      if (_movieData.length) {
        CCtzn.ui.renderMoviePicker(_movieData);
      }
      else {
        CCtzn.ui.cinemaPickedErrorHandler(['timeTooLate']);
      }
    }
  }

  function _cinemaPicked(cinemaData, startData) {
    var fileName = _getFilename(cinemaData, startData.date);
    if (fileName === _cachedFileName) {
      var movieData = JSON.parse(_cachedMovieData);
      _cinemaPickedSuccessHandler(movieData, startData);
    }
    else {
      $.ajax({
        url: fileName,
        dataType: 'json',
        dataFilter: function(data) {
          _cachedMovieData = data;
          _cachedFileName = fileName;
          return data;
        },
        success: function(movieData) {
          _cinemaPickedSuccessHandler(movieData, startData);
        },
        error: function() {
          CCtzn.ui.cinemaPickedErrorHandler(['dataNotFound']);
        }
      });
    }
  }

  // Plan data

  var _planData = [];

  function _isSelected(item, index) {
    return (this['ids'].indexOf(index) !== -1);
  }

  function _generateTimetable(selectedMovies, expertMode) {
    var	selectedMovieData = _movieData.filter(_isSelected, selectedMovies);

    if (selectedMovieData.length === 1) {
      _planData = CCtzn.calc.getSingleMoviePlan(selectedMovieData);
      CCtzn.ui.renderSingleMoviePlan(_planData);
    }
    else {
      _planData = CCtzn.calc.getCalculatedPlans(selectedMovieData, expertMode);
      CCtzn.ui.renderCalculatedPlan(_planData);
    }
  }

  function _generateAlternativeTimetable(set) {
    CCtzn.ui.renderCalculatedPlan(_planData, set, true);
  }

  // Timeborders

  var _timeBorders = {};

  function _getTimeBorders() {
    return _timeBorders;
  }

  function _setTimeBorders(max) {
    _timeBorders.min = new Date();
    _timeBorders.max = new Date(max);

    CCtzn.ui.renderDatePicker();
  }

  return {
    cinemaPicked: _cinemaPicked,
    generateTimetable: _generateTimetable,
    generateAlternativeTimetable: _generateAlternativeTimetable,
    getTimeBorders: _getTimeBorders,
    setTimeBorders: _setTimeBorders
  }

}());
