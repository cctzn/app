CCtzn.options = (function() {

  /*
   *	Option keys (all prefixed with 'option_'):
   *	Key                           Storage type    Description                             UI Effect
   *
   *	'expertmode_alwaysenabled'    Local           -                                       Hide "Expert mode" checkbox
   *	'recentcinema_disabled'       Local           Disable memory (and clear if checked)   -
   *	'recentcinema_clear'          Local           Clear recent cinema memory              None
   */

  var	$controls = $('#options').find('[data-option]'),
    _sessionKeys = [];

  function _getValue(key) {
    return JSON.parse(localStorage.getItem('option_' + key)) || JSON.parse(sessionStorage.getItem('option_' + key));
  }

  function _setOption(key, value) {
    var storage = (_sessionKeys.indexOf(key) === -1) ? 'localStorage' : 'sessionStorage';

    switch (key) {
      case 'recentcinema_disabled':
        if (value) {
          CCtzn.recent.clearState();
        }
        window[storage].setItem('option_' + key, value);
        break;
      case 'recentcinema_clear':
        CCtzn.recent.clearState();
        break;
      default:
        window[storage].setItem('option_' + key, value);
    }
  }

  function _init() {
    $controls.on('click', function() {
      var	$this = $(this),
        key = $this.data('option'),
        value = null;

      if ($this.is('[type=checkbox]')) {
        value = $this.prop('checked');
      }

      _setOption(key, value);
    });
  }

  return {
    getValue: _getValue,
    init: _init
  }

}());

CCtzn.options.init();
