module.exports = function(grunt) {
	
	var defaultTasks = ['concat', 'uglify'];
	
	grunt.initConfig({
	
		pkg: grunt.file.readJSON('package.json'),
		
		concat: {
			prod: {
				src: [
					'js/cctzn.base.js',
					'js/cctzn.config.js',
					'js/cctzn.calc.js',
					'js/cctzn.recent.js',
					'js/cctzn.options.js',
					'js/cctzn.achievements.js',
					'js/cctzn.ui.js'
				],
				dest: 'tmp/cctzn.js'
			}
		},
		
		uglify: {
			options: {
				mangle: false,
				preserveComments: false
			},
			prod: {
				files: {
					'www/js/cctzn.min.js': ['<%= concat.prod.dest %>']
				}
			}
		},
		
		watch: {
			files: ['<%= concat.prod.src %>'],
			tasks: defaultTasks
		}
	
	});
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	grunt.registerTask('default', defaultTasks);

};