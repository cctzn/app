## About

This is source code of Cinema Citizen web application ([website](http://cctzn.leibrug.pl)) I made for planning movie marathons, i.e. generating timetable from picked films with the shortest breaks between screenings. The application covers 130 cinemas of 4 major chains in Poland (Cinema City, Multikino, Helios and Cinema 3D). Calculation runs on client-side, based on JSON data, with kind-of [greedy algorithm](https://en.wikipedia.org/wiki/Greedy_algorithm).

## Running

1. Run `npm install` and `grunt`
2. Make directory `www/data`
3. Run make_timetables.php from [timetables-maker](https://gitlab.com/cctzn/timetables-maker) submodule - this will generate some JSON files with screenings data and timeborder.js file necessary for rendering date picker
4. Start application, for example using [http-server](https://www.npmjs.com/package/http-server): `http-server ./www`
